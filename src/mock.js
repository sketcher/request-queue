export const mock = function () {
    return {
        createRandomTimeOutPromise: (returnData = {}) => {
            const guid = Math.random().toFixed(10).slice(2)
            const timeout = [750, 1000, 2000, 3000, 4000, 5000, 1000, 1500, 2000].sort(() => 0.5 < Math.random())[0];
            return new Promise((resolve, reject) => {
                return new setTimeout(() => resolve({ guid, timeout, body: returnData }), timeout)
            })
        }
    }
}