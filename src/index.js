import { mock } from "./mock.js";
import Queue from "./queue.js";
import RequestQueue from "./requestQueue.js";

const queue = new RequestQueue()

for (let i = 0; i < 10; i++) {
    queue.enqueuePromise(i)
}


// const act = () => {
//     if (queue.length() < 1) {
//         return;
//     }
//     // const item = queue.first();
//     // const promise = item.createRandomTimeOutPromise()
//     // promise.then(res => {
//     //     console.log(res, { promise })
//     //     queue.dequeue()
//     //     act()
//     // })
//     //     .catch(err => {
//     //         console.error(err)
//     //         queue.dequeue()
//     //         act()
//     //     })
// }
queue.onFinishedAct = () => {
    console.log('Me is done')
}
queue.onFinishedBatch = (values) => {
    console.log('batch', values)
}

for (let i = 0; i < 10; i++) {
    queue.enqueuePromise(i)
}
queue.actWithBatch()
