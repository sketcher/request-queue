export default class Queue {
    constructor() {
        this.offset = 0
        this.data = new Map()
    }

    enqueue(item) {
        const current = this.offset + this.length()
        this.data.set(current, item)
    }

    dequeue() {
        if (this.length() > 0) {
            this.data.delete(this.offset)
            this.offset += 1
        }
    }

    batch(size = 3) {
        let items = []
        if (this.length() < 1) {
            return []
        }
        while(size-- > 0){
            if(this.length() < 1){
                break;
            }
            items.push(this.first())
            this.dequeue()
        }
        return items
    }

    first() {
        return this.data.get(this.offset)
    }

    last() {
        return this.data.get(this.offset + this.length() - 1)
    }

    length() {
        return this.data.size
    }
}