import Queue from "./queue.js";
import { mock } from "./mock.js";


export default class RequestQueue extends Queue {
    onFinishedAct = () => { }
    onFinishedBatch = () => { }
    constructor() {
        super()
    }
    enqueuePromise(id) {
        this.enqueue(() => {
            return mock().createRandomTimeOutPromise(id)
        })
        // this.enqueue(function(){
        //     return new Promise((res) => setTimeout(res(id),100))
        // })
    }

    act() {
        console.log('calling act()')
        if (this.length() < 1) {
            this.onFinishedAct()
            return;
        }

        const item = this.first()
        console.log('#', item)
        item().then(res => {
            console.log('got:', res)
            this.dequeue()
            this.act()
        })
    }

    actWithBatch(batchSize = 3) {
        if (this.length() < 1) {
            this.onFinishedAct()
            return;
        }

        const batch = this.batch(batchSize)
        console.log('#batch', batch)

        Promise.allSettled(batch.map(x => x()))
            .then(res => {
                // console.log('got:', res.map(y => y.value.body))
                this.onFinishedBatch(res.map(y => y.value.body))
                this.actWithBatch(batchSize)
            })
    }
}